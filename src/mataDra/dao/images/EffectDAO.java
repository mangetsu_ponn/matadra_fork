package mataDra.dao.images;

import java.util.List;

import mataDra.dao.DAO;
import mataDra.entity.images.EffectImageEntity;
import mataDra.entity.images.EffectImageEntity.EffectType;

public class EffectDAO extends DAO<EffectImageEntity> {

    @Override
    public List<EffectImageEntity> registerAll() {
        // TODO 自動生成されたメソッド・スタブ

    	setEntity(new EffectImageEntity(0, "単体攻撃1", "〆",EffectType.SINGLE));
        setEntity(new EffectImageEntity(1, "単体攻撃スキル1", "火",EffectType.SINGLE));
        setEntity(new EffectImageEntity(2, "全体攻撃1", "》",EffectType.MULTI));
        setEntity(new EffectImageEntity(3, "全体攻撃スキル1", "火",EffectType.MULTI));
		return getEntity();

    }

}
